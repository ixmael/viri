(function () {

    var quoteSliderController = function () {

        var historySlider = $(".quote-wrapper-slider");
        var totalQuotes = $('.quote-wrapper-slider .quote-item').length;

        if (historySlider.length > 0) {

            historySlider.slick({
                dots: false,
                arrows: false,
                centerMode: true,
                centerPadding: '310px',
                slidesToShow: 2,
                infinite: false,
                draggable: true,
                cssEase: 'ease-in-out',
                responsive: [
                    {
                        breakpoint: 1001,
                        settings: {
                            centerPadding: '0px',
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 801,
                        settings: {
                            centerPadding: '80px',
                            slidesToShow: 1
                        }
                    }
                ]
            });

            $('.testimonials-controls .next').click(function () {
                if ( $(this).hasClass('disabled') ) return;
                historySlider.slick('slickNext');
            });

            $('.testimonials-controls .prev').click(function () {
                if ( $(this).hasClass('disabled') ) return;
                historySlider.slick('slickPrev');
            });

            if ($(window).innerWidth() >= 900) {
                historySlider.slick('slickNext');

                historySlider.on('afterChange', function (event, slick, currentSlide, nextSlide) {

                    if (currentSlide == 1) {
                        $('.testimonials-controls .prev').addClass('disabled');
                        $('.testimonials-controls .next').removeClass('disabled');
                    } else if (currentSlide == totalQuotes - 2) {

                        $('.testimonials-controls .prev').removeClass('disabled');
                        $('.testimonials-controls .next').addClass('disabled');
                        
                    } else {
                        $('.testimonials-controls .prev, .testimonials-controls .next').removeClass('disabled');
                    }
                });

            }else{
                historySlider.slick('slickPrev');

                historySlider.on('afterChange', function (event, slick, currentSlide, nextSlide) {
                    if (currentSlide == 0) {
                        $('.testimonials-controls .prev').addClass('disabled');
                    } else if (currentSlide == totalQuotes - 1) {
                        $('.testimonials-controls .next').addClass('disabled');
                    } else {
                        $('.testimonials-controls .prev, .testimonials-controls .next').removeClass('disabled');
                    }
                });

            }
        }
    };

    var scrollHeaderController = function(){

        $(window).scroll(function() {
			var header = $('header'),
				headerHeight = header.height();
			if ($(window).scrollTop() >= headerHeight) {
				header.addClass('change');
			} else {
				header.removeClass('change');
			}
		});
    };

    var scrollToController = function(){

        $('.cta').on('click', function (e){

            var topDistance = $('.contact-block').position().top - 50;

            e.preventDefault();
            $('html, body').animate({scrollTop: topDistance}, 'slow');
        });
    };    

    $(document).on('ready', function () {

        scrollHeaderController();
        scrollToController();
        quoteSliderController();
    });
	
})();